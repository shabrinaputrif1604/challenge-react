import logo from './logo.svg';
import './App.css';
import { Route, Routes } from "react-router-dom";
import Home from './pages/Home';
import './css/style.css'
// import './css/style-cari-mobil.css'
import FindCar from './pages/FindCar';

function App() {
  return (
    <div className="App">

      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/findCar" element={<FindCar />} />
      </Routes>
    </div>
  );
}

export default App;
