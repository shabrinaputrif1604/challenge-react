import React, { Component } from 'react';
import TopContent from '../components/Content/TopContent';
import ImgService from '../images/img_service.png'
import ImgCheck from '../images/Group53.png'
import ImgComplete from '../images/icon_complete.png'
import ImgPrice from '../images/icon_price.png'
import ImgClock from '../images/icon_24hrs.png'
import ImgProfessional from '../images/icon_professional.png'
import ImgProfPic from '../images/img_photo.png'
import ImgRate from '../images/rate.png'
import Footer from '../components/Content/Footer';
import Navbar from '../components/Navbar/Navbar';

class Home extends Component {
    render() {
        return (
            <div>
                <>
                <div className='container-fluid'>
                <Navbar />
                <TopContent />

                {/* Our Service */}
                <div class="our-service" id="our-service">
                    <div class="row">
                        <div class="col-6">
                            <img src={ImgService} alt="mbakmbak" class="woman-img" />
                        </div>
                        <div class="col-6 service-content">
                            <p class="menu-name">Best Car Rental for any kind of trip in Palembang!</p>
                            <p>Sewa mobil di Palembang bersama Binar Car Rental jaminan harga lebih murah dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding, meeting, dll.</p>
                            <p><img src={ImgCheck} alt="" class="check-img" />Sewa Mobil Dengan Supir di Palembang 12 Jam</p>
                            <p><img src={ImgCheck} alt="" class="check-img" />Sewa Mobil Lepas Kunci di Palembang 24 Jam</p>
                            <p><img src={ImgCheck} alt="" class="check-img" />Sewa Mobil Jangka Panjang Bulanan</p>
                            <p><img src={ImgCheck} alt="" class="check-img" />Gratis Antar - Jemput Mobil di Bandara</p>
                            <p><img src={ImgCheck} alt="" class="check-img" />Layanan Airport Transfer / Drop In Out</p>
                        </div>
                    </div>
                </div>

                {/* Why Us */}
                <div class="why_us" id="why-us">
                    <div class="whyus-content">
                        <p class="menu-name">Why Us?</p>
                        <p>Mengapa harus pilih Binar Car Rental?</p>
                    </div>
                    <div class="row benefit">
                        <div class="card col-3">
                            <div class="card-body">
                            <img src={ImgComplete} alt="" />
                            <p class="whyus-title">Mobil Lengkap</p>
                            <p>Tersedia banyak pilihan mobil, kondisi masih baru, bersih dan terawat</p>
                            </div>
                        </div>
                        <div class="card col-3">
                            <div class="card-body">
                                <img src={ImgPrice} alt="" />
                                <p class="whyus-title">Harga Murah</p>
                                <p>Harga murah dan bersaing, bisa bandingkan harga kami dengan rental mobil lain</p>
                            </div>
                        </div>
                        <div class="card col-3">
                            <div class="card-body">
                                <img src={ImgClock} alt="" />
                                <p class="whyus-title">Layanan 24 Jam</p>
                                <p>Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami juga tersedia di akhir minggu</p>
                            </div>
                        </div>
                        <div class="card col-3">
                            <div class="card-body">
                            <img src={ImgProfessional} alt="" />
                            <p class="whyus-title">Sopir Profesional</p>
                            <p>Sopir yang profesional, berpengalaman, jujur, ramah dan selalu tepat waktu</p>
                            </div>
                        </div>
                    </div>
                </div>

                {/* Testimonials */}
                <div class="testimonial" id="testimonial">
                    <div class="testimonial-content">
                        <p class="menu-name">Testimonial</p>
                        <p>Berbagai review positif dari para pelanggan kami</p>
                    </div>
                    <div class="testimonial-card">
                        <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
                            <div class="carousel-inner">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="carousel-item">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                            <div class="col-3">
                                                <img src={ImgProfPic} alt="" class="photo" />
                                            </div>
                                            <div class="col-9">
                                                <img src={ImgRate} alt="" />
                                                <p>“Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod”</p>
                                                <p class="profile-name">John Dee 32, Bromo</p>
                                            </div>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            </div>
                        </div>
                        <div class="carousel-button">
                            <button class="carousel-control" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
                            <span class="carousel-control-prev-icon tombol-crsl" aria-hidden="true"></span>
                            <span class="visually-hidden">Previous</span>
                            </button>
                            <button class="carousel-control" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="next">
                            <span class="carousel-control-next-icon tombol-crsl" aria-hidden="true"></span>
                            <span class="visually-hidden">Next</span>
                            </button>
                        </div>
                    </div>
                </div>

                {/* Rent Car Area */}
                <div class="rent-car">
                    <div class="card rent-card">
                        <div class="card-body">
                            <p class="menu-name">Sewa Mobil di Palembang Sekarang</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                            <button type="button" class="btn btn-success">Mulai Sewa Mobil</button>
                        </div>
                    </div>
                </div>

                {/* FAQ */}
                <div class="faq" id="faq">
                    <div class="row">
                        <div class="col-4 faq-content">
                        <p class="menu-name">Frequently Asked Question</p>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
                        </div>
                        <div class="col-8 faq-question">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item">
                            <h2 class="accordion-header" id="headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                Apa saja syarat yang dibutuhkan?
                                </button>
                            </h2>
                            <div id="collapseOne" class="accordion-collapse collapse" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                Lorem, ipsum dolor sit amet consectetur adipisicing elit. Quis, inventore explicabo. Quibusdam temporibus dolores dolor illo, minus unde repellat ipsum perspiciatis veniam totam, magnam similique soluta cumque, mollitia laudantium magni?
                                </div>
                            </div>
                            </div>
                            <div class="accordion-item">
                            <h2 class="accordion-header" id="headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Berapa hari minimal sewa mobil lepas kunci?
                                </button>
                            </h2>
                            <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Delectus obcaecati at adipisci ut libero repellendus pariatur doloremque, fuga officiis perspiciatis nobis nostrum harum? Sed, tenetur doloremque quis enim vel minus?
                                </div>
                            </div>
                            </div>
                            <div class="accordion-item">
                            <h2 class="accordion-header" id="headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Berapa hari sebelumnya sebaiknya booking sewa mobil?
                                </button>
                            </h2>
                            <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Laudantium, veniam. Laudantium earum, nulla maxime sapiente vero ad soluta! Assumenda reprehenderit corrupti accusamus obcaecati veniam, nemo labore voluptate eveniet nesciunt libero.
                                </div>
                            </div>
                            </div>
                            <div class="accordion-item">
                            <h2 class="accordion-header" id="headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                Apakah ada biaya antar-jemput?
                                </button>
                            </h2>
                            <div id="collapseFour" class="accordion-collapse collapse" aria-labelledby="headingFour" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus optio, exercitationem quos et iusto, eum iste quis tempore magni ea voluptas eius nam, recusandae porro commodi vitae rerum nihil. Pariatur!
                                </div>
                            </div>
                            </div>
                            <div class="accordion-item">
                            <h2 class="accordion-header" id="headingFive">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                Bagaimana jika terjadi kecelakaan?
                                </button>
                            </h2>
                            <div id="collapseFive" class="accordion-collapse collapse" aria-labelledby="headingFive" data-bs-parent="#accordionExample">
                                <div class="accordion-body">
                                Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tenetur laborum assumenda quod iure dolorum modi? Voluptatem id, eos culpa ex excepturi nulla delectus nisi, reprehenderit recusandae cum, praesentium odit ipsum!
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>

                <Footer />
            </div>
            
            </>
            </div>
        );
    }
}

export default Home;


