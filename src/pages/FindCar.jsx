import React, { Component } from 'react';
import { connect } from 'react-redux';
// import Card from '../components/Card/Card';
import Cards from '../components/Card/Cards';
import Footer from '../components/Content/Footer';
import Navbar from '../components/Navbar/Navbar';
import ImgCar from '../images/img_car.png';
import { allCars } from '../store/actions/CarsAction'

class FindCar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            availableAt : '',
            capacity : 0
        }
    }

    handleDate = (event) => {
        this.setState({
            availableAt : event.target.value
        })
    }

    handleTime = (event) => {
        this.setState({
            time : event.target.value
        })
    }

    handlePassenger = (event) => {
        this.setState({
            capacity : event.target.value
        })
    }

    handleButton = (event) => {
        let objForm = {
            availableAt : this.state.availableAt,
            capacity :this.state.capacity,
            time : this.state.time
        }
        
        this.props.allCars(objForm)
        console.log(objForm);
    }

    render() {
        return (
            <div>
                <Navbar />
                <div class="top-content">
                <div class="row">
                    <div class="col-6 description">
                        <h1>Sewa dan Rental Mobil Terbaik di Kawasan Palembang</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas 
                            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu 
                            untuk sewa mobil selama 24 jam.</p>
                    </div>
                        <div class="col-6 car_image">
                            <img src={ImgCar} alt="mobil" />
                        </div>
                    </div>
                </div>
                
                <div class="cari-mobil container-fluid">
                    <div class="box">
                        <div class="row">
                            <div class="col-2 tipe-driver">
                                <label>Tipe Driver</label>
                                <select class="form-select form-select-md" id="tipeDriver" aria-label=".form-select-md example">
                                <option disabled selected>Pilih Tipe Driver</option>
                                <option value="denganSopir">Dengan Sopir</option>
                                <option value="tanpaSopir">Tanpa Sopir (Lepas kunci)</option>
                                </select>
                            </div>
                            <div class="col-2 tanggal">
                                <label>Tanggal</label>
                                <div class="input-group-md">
                                <input type="date" id="tanggal" name="tanggal" class="form-control" onChange={(event) => this.handleDate(event)} />
                                </div>
                            </div>
                            <div class="col-3 waktu">
                                <label>Waktu Jemput/Ambil</label><br />
                                <select class="form-select form-select-md" id="waktuJemput" aria-label=".form-select-md example" 
                                onChange={(event) => this.handleTime(event)} >
                                <option disabled selected>Pilih Waktu Jemput</option>
                                <option value="08:00">08:00 WIB</option>
                                <option value="09:00">09:00 WIB</option>
                                <option value="10:00">10:00 WIB</option>
                                <option value="11:00">11:00 WIB</option>
                                <option value="12:00">12:00 WIB</option>
                                </select>
                            </div>
                            <div class="col-3 penumpang">
                                <label>Penumpang</label>
                                <div class="input-group-md">
                                <input type="number" class="form-control" id="jumlahPenumpang" placeholder="Jumlah Penumpang" onChange={(event) => this.handlePassenger(event)}/>
                                </div>
                            </div>
                            <div class="col-2 btn-submit">
                                <button class="btn form-control" 
                                id="submitFilter" 
                                value="Cari Mobil"
                                onClick={(event) => this.handleButton(event)} >
                                    Cari Mobil
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <div className='row card-car'>
                    <Cards />
                </div>

                <Footer />
            </div>
        );
    }
}

export default connect(null, {allCars})(FindCar);


