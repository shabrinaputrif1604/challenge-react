import { combineReducers } from "redux";
import carsReducers from "./CarsReducer";

const index = combineReducers({
    cars : carsReducers
})

export default index