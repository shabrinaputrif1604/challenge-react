import { ALL_CARS } from "../actions/CarsAction";

export default function carsReducers(state = {cars: []}, action) {
    switch (action.type) {
        case ALL_CARS:
            return {...state, cars: action.payload}
        default:
            return state;
    }
}