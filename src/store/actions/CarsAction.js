import axios from "axios"

export const ALL_CARS = "ALL_CARS"

export const allCars = ({capacity,availableAt,time}) => async dispatch => {
   try {
        const response = await axios.get('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json')
        // console.log({capacity, availableAt: Date.parse(new Date(availableAt))});
        if(capacity !== "" && availableAt !== "" && time !== ""){
            let dateTime = availableAt + "T" + time
            const data = response.data.filter(item => {
                // return parseInt(item.capacity) >= parseInt(capacity) && Date.parse(new Date(item.availableAt)) <= new Date(dateTime) && item.available === true
                return (Date.parse(item.availableAt) <= Date.parse(dateTime)) && (parseInt(item.capacity) >= parseInt(capacity));
            })
            dispatch({
                type: ALL_CARS,
                payload: data
            });
        }else if(availableAt !== "" && time !== ""){
            let dateTime = availableAt + "T" + time
            const data = response.data.filter(item => {
                // return parseInt(item.capacity) >= parseInt(capacity) && Date.parse(new Date(item.availableAt)) <= new Date(dateTime) && item.available === true
                return (Date.parse(item.availableAt) <= Date.parse(dateTime));
            })
            dispatch({
                type: ALL_CARS,
                payload: data
            });
        }else if(capacity !== ""){
            const data = response.data.filter(item => {
                // return parseInt(item.capacity) >= parseInt(capacity) && Date.parse(new Date(item.availableAt)) <= new Date(dateTime) && item.available === true
                return (parseInt(item.capacity) >= parseInt(capacity));
            })
            dispatch({
                type: ALL_CARS,
                payload: data
            });    
        }else{
            dispatch({
                type: ALL_CARS,
                payload: response.data
            });
        }
        // const data = response.data
        // dispatch({
        //     type: ALL_CARS,
        //     payload: data
        // });
   } catch (error) {
       console.log(error);
   }
}
