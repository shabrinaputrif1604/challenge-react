import React, { Component } from 'react';
import ImgUsers from '../../images/fi_users.png'
import ImgSettings from '../../images/fi_settings.png'
import ImgCalendar from '../../images/fi_calendar.png'

class Card extends Component {

    constructor(props) {
        super(props);

        this.state = {
            list_cars_data : []
        }
    }

    componentDidMount() {
        fetch('https://raw.githubusercontent.com/fnurhidayat/probable-garbanzo/main/data/cars.min.json').then((res) => res.json())
        .then((data) => {
            this.setState({
                list_cars_data : data
            })
        })
    }

    render() {
        return (
            <>
                {this.state.list_cars_data.map((item) => 
                    <div className='col-4'>
                        <div class="card mb-4">
                    
                        <img src={item.image.replace("./", "https://res.cloudinary.com/sriwijaya-university/image/upload/v1653574348/")} class="card-img-top img-fluid" alt="..." />
                        <p><b>{item.manufacture} / {item.model}</b></p>
                        <p><b>{item.rentPerDay}/hari</b></p>
                        <p class="card-text">{item.description}</p>
                        <p><img src={ImgUsers} alt="" class="ikon" />{item.capacity}</p>
                        <p><img src={ImgSettings} alt="" class="ikon" />{item.transmission}</p>
                        <p><img src={ImgCalendar} alt="" class="ikon" />Tahun {item.year}</p>
                        <button class="btn btn-success">Pilih Mobil</button>
                
                        </div>
                    </div> 
                )}
            </>
                
        );
    }
}

export default Card;

