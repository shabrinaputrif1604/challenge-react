import React, { Component } from 'react';
// import Link from "react-router-dom";

class Navbar extends Component {
    render() {
        return (
            <>
                <nav class="navbar navbar-expand-lg fixed-top">
                    <div class="container-fluid">
                    <a class="navbar-brand logo"></a>
                    {/* <Link to={'/'}></Link> */}
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div class="navbar-nav nav-menu">
                        <a class="nav-link active" aria-current="page" href="#our-service">Our Service</a>
                        <a class="nav-link active" href="#why-us">Why Us</a>
                        <a class="nav-link active" href="#testimonial">Testimonial</a>
                        <a class="nav-link active" href="#faq">FAQ</a>
                        <button type="button" class="btn btn-register">Register</button>
                        </div>
                    </div>
                    </div>
                </nav>
            </>
        );
    }
}

export default Navbar;
