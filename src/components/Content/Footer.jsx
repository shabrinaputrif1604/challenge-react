import React, { Component } from 'react';
import ImgFB from '../../images/icon_facebook.png'
import ImgIG from '../../images/icon_instagram.png'
import ImgTwitch from '../../images/icon_twitch.png'
import ImgTwitter from '../../images/icon_twitter.png'
import ImgMail from '../../images/icon_mail.png'
import ImgBrand from '../../images/rectangle74.png'

class Footer extends Component {
    render() {
        return (
            <div>
                <footer className='container-fluid'>
                <div class="row">
                    <div class="col-4 address">
                        <p>Jalan Suroyo No. 161 Mayangan Kota Palembang 672000</p>
                        <p>binarcarrental@gmail.com</p>
                        <p>081-233-334-808</p>
                    </div>
                    <div class="col-2">
                        <p><a href="#our-service">Our services</a></p>
                        <p><a href="#why-us">Why Us</a></p>
                        <p><a href="#testimonial">Testimonial</a></p>
                        <p><a href="#faq">FAQ</a></p>
                    </div>
                    <div class="col-3">
                    <p>Connect with us</p>
                        <a href=""><img src={ImgFB} alt="" /></a>
                        <a href=""><img src={ImgIG} alt="" /></a>
                        <a href=""><img src={ImgTwitter} alt="" /></a>
                        <a href=""><img src={ImgMail} alt="" /></a>
                        <a href=""><img src={ImgTwitch} alt="" /></a>  
                    </div>
                    <div class="col-3">
                        <p>Copyright</p>
                        <a href=""><img src={ImgBrand} alt="" /></a>
                    </div>
                </div>
            </footer>
            </div>
        );
    }
}

export default Footer;
