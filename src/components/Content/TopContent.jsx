import { Link } from "react-router-dom";
import React, { Component } from 'react';
import ImgCar from "../../images/img_car.png";

class TopContent extends Component {
    render() {
        return (
            <div class="top-content">
                <div class="row">
                    <div class="col-6 description">
                        <h1>Sewa dan Rental Mobil Terbaik di Kawasan Palembang</h1>
                        <p>Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas 
                            terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu 
                            untuk sewa mobil selama 24 jam.</p>
                        <button type="button" class="btn btn-success"><Link to={'/findCar'}>Mulai Sewa Mobil</Link></button>
                    </div>
                    <div class="col-6 car_image">
                        <img src={ImgCar} alt="mobil" />
                    </div>
                </div>
            </div>
        );
    }
}

export default TopContent;
